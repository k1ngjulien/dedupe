import dedupe


EXPECTED_HASH = "e5fa44f2b31c1fb553b6021e7360d07d5d91ff5e"


def test_calculateHash():
    hash = dedupe.calculateHash("test/test1.txt")
    assert hash == EXPECTED_HASH


def test_findDuplicates():
    index = dedupe.findDuplicates("test/")
    file = dedupe.File(0, "test/test1.txt")

    assert len(index[EXPECTED_HASH]) == 4
    assert file in index[EXPECTED_HASH]


def test_shouldExcude():
    exclude = ["test/exc/"]
    filepath = "test/exc/test1_exc.txt"

    assert dedupe.shouldExclude(filepath, exclude) == True
