# Dedupe

Tool to find and remove duplicate files via sha1 hash.

```
usage: ./dedupe.py [-h] [-e EXCLUDE [EXCLUDE ...]] [-c | --cleanup | --no-cleanup] root_dir

identify and clean up duplicate files

positional arguments:
  root_dir              root of subdirectories to be searched

options:
  -h, --help            show this help message and exit
  -e EXCLUDE [EXCLUDE ...], --exclude EXCLUDE [EXCLUDE ...]
                        directories to exclude
  -c, --cleanup, --no-cleanup
                        remove duplicates. least deep duplicate will be kept
```


## Example Usage

### Example directory structure

Files starting with `test1*` have the same contents.

```console
[~/code/dedupe]: tree test/
test/
├── exc
│   └── test1_exc.txt
├── sub
│   └── test1_dup2.txt
├── test1_dupe.txt
├── test1.txt
└── test2.txt

3 directories, 5 files
```

### Finding Duplicates

```console
[~/code/dedupe]: ./dedupe.py test/
e5fa44f2b3: test/test1.txt
e5fa44f2b3: test/test1_dupe.txt
e5fa44f2b3: test/exc/test1_exc.txt
e5fa44f2b3: test/sub/test1_dup2.txt
4 duplicates found, 0 files excluded, 0 files deleted
```

### Remove Duplicates
```console
[~/code/dedupe]: ./dedupe.py test/ --cleanup
e5fa44f2b3: test/test1.txt
e5fa44f2b3: test/test1_dupe.txt deleting...
e5fa44f2b3: test/exc/test1_exc.txt deleting...
e5fa44f2b3: test/sub/test1_dup2.txt deleting...
4 duplicates found, 0 files excluded, 3 files deleted
[~/code/dedupe]: tree test/
test/
├── exc
├── sub
├── test1.txt
└── test2.txt

3 directories, 2 files
```

### Exclude certain directories

```console
[~/code/dedupe]: ./dedupe.py test/ -e test/exc/
e5fa44f2b3: test/test1.txt
e5fa44f2b3: test/test1_dupe.txt
e5fa44f2b3: test/exc/test1_exc.txt excluding...
e5fa44f2b3: test/sub/test1_dup2.txt
4 duplicates found, 1 files excluded, 0 files deleted
[~/code/dedupe]: ./dedupe.py test/ -e test/exc/ -c
e5fa44f2b3: test/test1.txt
e5fa44f2b3: test/test1_dupe.txt deleting...
e5fa44f2b3: test/exc/test1_exc.txt excluding...
e5fa44f2b3: test/sub/test1_dup2.txt deleting...
4 duplicates found, 1 files excluded, 2 files deleted
[~/code/dedupe]: tree test/
test/
├── exc
│   └── test1_exc.txt
├── sub
├── test1.txt
└── test2.txt

3 directories, 3 files
```

## Performance

This is by no means a comprehensive benchmark but it should demonstrate that this is plenty fast, crunching 11GB of hashes in 12 seconds 👍.

Run on an `Intel(R) Core(TM) i7-8565U CPU @ 1.80GHz` with an NVMe SSD capable of 1,6GB/s sequential read.

```console
[~/code/dedupe]: du -sh ~/OneDrive/
11G     /home/julian/OneDrive/
[~/code/dedupe]: time ./dedupe.py ~/OneDrive/
...
6471 duplicates found, 0 files excluded, 0 files deleted

real    0m12,020s
user    0m9,978s
sys     0m1,807s
```

The code looks to be mostly CPU-Bound since this is all single threaded. Will look into doing this multithreaded soon(TM).

