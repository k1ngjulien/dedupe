#!/usr/bin/env python3

import os
import sys
import hashlib
import argparse
from dataclasses import dataclass


@dataclass
class File:
    depth: int
    path: str


def getArgs():
    parser = argparse.ArgumentParser(
        prog=sys.argv[0],
        description='identify and clean up duplicate files'
    )

    parser.add_argument(
        "root_dir", help="root of subdirectories to be searched")
    parser.add_argument(
        "-e", "--exclude",
        default=[],
        action="extend", nargs="+", type=str,
        help="directories to exclude")
    parser.add_argument(
        "-c", "--cleanup",
        action=argparse.BooleanOptionalAction,
        help="remove duplicates. least deep duplicate will be kept")

    return parser.parse_args()


def main():
    args = getArgs()

    index = findDuplicates(args.root_dir)

    duplicates, excluded, deleted = 0, 0, 0
    for fhash in index:
        files = sorted(index[fhash], key=lambda x: x.depth)

        # skip files with no found duplicates
        if len(files) <= 1:
            continue

        for i, f in enumerate(files):
            print(f"{fhash[:10]}: {f.path}", end="")

            duplicates += 1

            if shouldExclude(f.path, args.exclude):
                print(" excluding...")
                excluded += 1
                continue

            if args.cleanup and i != 0:
                print(" deleting...", end="")
                deleted += 1
                os.remove(f.path)

            print()

    print(f"{duplicates} duplicates found, {excluded} files excluded, {deleted} files deleted")


def findDuplicates(root_dir: str):
    index: dict[str, list[File]] = {}
    root_dir = root_dir.rstrip("/")

    for dirpath, _, filenames in os.walk(root_dir):

        depth = dirpath.count("/")
        for name in filenames:
            path = os.path.join(dirpath, name)
            fhash = calculateHash(path)

            foudPaths = index.get(fhash, [])
            foudPaths.append(File(depth, path))
            index[fhash] = foudPaths

    return index


def shouldExclude(path: str, excludePaths: list[str]):
    for e in excludePaths:
        if path.startswith(e):
            return True

    return False


def calculateHash(filepath):
    BUF_SIZE = 64 * 2**10

    sha1 = hashlib.sha1()

    with open(filepath, 'rb') as f:
        while True:
            data = f.read(BUF_SIZE)
            if not data:
                break
            sha1.update(data)

    return sha1.hexdigest()


if __name__ == "__main__":
    main()
